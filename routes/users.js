var express = require('express');
var router = express.Router();
var moment = require('moment');
var request = require("request");
var http = require("http");
var async = require("async");
var geolib = require("geolib");

//Customer sign_up
router.post('/create', function (req, res) {
    var Fname = req.body.firstname;
    var Lname = req.body.lastname;
    var email = req.body.email;
    var password = req.body.password;
    var phone = req.body.phone;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var checkBlank = [Lname, Fname, email, password, phone, deviceToken, deviceType, latitude, longitude];
    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, checkBlank, callback);
            },
            function (callback) {
                func.checkEmailAvailability(res, email, 1, callback);
            }],
        function (updatePopup) {
            func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {
                var hash = md5(password);
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + email;
                var accessToken = md5(accesstoken1);
                var loginTime = new Date();
                var sql = "INSERT into tb_users(Fname,Lname,email,encryptPassword,phone,access_token,date_registered,last_login,device_token,device_type,latitude,longitude,file_path) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [Fname, Lname, email, hash, phone, accessToken, loginTime, loginTime, deviceToken, deviceType, latitude, longitude, result_file,], function (err, result) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var sql = "SELECT * FROM tb_users WHERE email=? LIMIT 1";
                        connection.query(sql, [email], function (err, user, fields) {
                            var final = {
                                "Fname": user[0].Fname,
                                "Lname": user[0].Lname,
                                "email": user[0].email,
                                "user_id": user[0].user_id,
                                "last_login": user[0].last_login,
                                "password": user[0].encryptPassword,
                                "phone": user[0].phone,
                                "access_token": user[0].access_token,
                                "date_registered": user[0].date_registered,
                                "device_token": user[0].device_token,
                                "device_type": user[0].device_type,
                                "latitude": user[0].latitude,
                                "longitude": user[0].longitude
                            };
                            sendResponse.sendSuccessData(final, res);
                            var userName = user[0].Fname;
                            func.sendMailToCustomer(userName, email, res);
                        });
                    }
                });
            });
        });
});

//Customer login
router.post('/login', function (req, res) {
    var user = req.body.user;
    var pass = req.body.pass;
    var check = [user, pass];
    var flag = func.checkBlank(check);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {
        func.email_login_function(user, pass, 1, res);
    }
});

//Customer access token login
router.post('/access_token_login', function (req, res) {
    var accessToken = req.body.access_token;
    var manvalues = [accessToken];
    var checkdata = func.checkBlank(manvalues);
    if (checkdata === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id`,`device_type`,`device_token`,`Fname`,`Lname`,`file_path`,`phone`,`email`,`encryptPassword`,`date_registered`,`device_type`,`device_token` FROM `tb_users` WHERE `access_token`=? LIMIT 1 "
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length !== 0) {
                var md5 = require('MD5');
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + result[0].email;
                var accessToken2 = md5(accesstoken1);
                var sql = "UPDATE `tb_users` set `access_token`=?,`login_status`=? WHERE `user_id`=? LIMIT 1 "
                connection.query(sql, [accessToken2, 1, result[0].user_id], function (err, resultUpdate) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var final = {
                            "user_name": result[0].Fname,
                            "user_image": result[0].user_image,
                            "access_token": result[0].access_token,
                            "email": result[0].email,
                            "phone_no": result[0].phone,
                            "device_token": result[0].device_token,
                            "device_type": result[0].device_type,
                            "user_id": result[0].user_id,
                            "last_login": result[0].last_login,
                            "password": result[0].encryptPassword,
                            "phone": result[0].phone,
                            "date_registered": result[0].date_registered,
                            "latitude": result[0].latitude,
                            "longitude": result[0].longitude
                        };
                        sendResponse.sendSuccessData(final, res);
                    }
                });
            }
            else {
                sendResponse.invalidAccessTokenError(res);
            }
        });
    }
});

//Customer logout
router.post('/logout', function (req, res, next) {
    var accessToken = req.body.access_token;
    var checkData = func.checkBlank(accessToken);
    if (checkData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `login_status` FROM `tb_users` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length === 0) {
                sendResponse.invalidAccessTokenError(res);
            }
            else {
                var sql = "UPDATE `tb_users` SET `login_status`=? WHERE `access_token`=? LIMIT 1";
                connection.query(sql, [0, accessToken], function (err, result) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        sendResponse.sendSuccessData(constant.responseMessage.SUCCESSFUL_LOGOUT, res);
                    }
                });
            }
        });
    }
});

//Customer edit profile
router.post('/edit_profile', function (req, res) {
    var accessToken = req.body.access_token;
    var phone = req.body.phone;
    var email = req.body.email;
    var firstName = req.body.Fname;
    var lastName = req.body.Lname;
    var values = [accessToken, phone, firstName, lastName, email];
    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id`FROM `tb_users` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {
                    file = req.files.user_image;
                    filename = file.name;
                    remotePath = config.get("production").Url + filename;
                    filePath = config.get("production").Url;
                    if (remotePath === filePath) {
                        var sql1 = "UPDATE `tb_users` SET `Fname`=?,`Lname`=?,`phone`=?,`email`=? WHERE `user_id`=? LIMIT 1"
                        connection.query(sql1, [firstName, lastName, phone, email, result[0].user_id], function (err, result) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                sendResponse.sendSuccessData(constant.responseMessage.UPDATE_PROFILE, res);
                            }
                        });
                    }
                    else {
                        func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {
                            var sql2 = "UPDATE `tb_users` SET `file_path`=?,`Fname`=?,`Lname`=?,`phone`=?,`email`=?WHERE `user_id`=? LIMIT 1 ";
                            connection.query(sql2, [result_file, firstName, lastName, phone, email, result[0].user_id], function (err, result_update) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    sendResponse.sendSuccessData(constant.responseMessage.UPDATE_PROFILE, res);
                                }
                            });
                        });
                    }
                }
            }
        });
    }
});
//Customer forget password
router.post('/forgot', function (req, res) {
    var email = req.body.email;
    var manValues = [email];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        },
        function (callback) {
            func.checkEmailRegistration(res, email, 1, callback);
        }], function (updatePopup) {
        var sql = "SELECT user_id,Fname FROM tb_users WHERE email=? LIMIT 1";
        connection.query(sql, [email], function (err, response12) {
            if (response12.length === 1) {
                func.sendPasswordMailToCustomer(email, 1, res);
            }
            else {
                sendResponse.sendError(res);
            }
        });
    });
});

//Customer reset password
router.post('/reset', function (req, res) {
    var link = req.body.link;
    var new_password = req.body.pass;
    var manValues = [link, new_password];
    var checkData = func.checkBlank(manValues);
    if (checkData === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id` FROM `tb_users` WHERE `one_time_link`=? LIMIT 1"
        connection.query(sql, [link], function (err, result5) {
            if (result5) {
                var md5 = require('MD5');
                var hash = md5(new_password);
                var sql = "SELECT `password_reset` FROM `tb_users` WHERE `one_time_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result57) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        if (result57.length == 0) {
                            sendResponse.sendSuccessData(constant.responseMessage.SHOW_LINK_ERROR_MESSAGE, res);
                        }
                        else if (result57[0].password_reset == 0) {
                            var sql2 = "UPDATE `tb_users` SET `one_time_link`=?,`encryptPassword`=?,`password_reset`=? WHERE `user_id`=? LIMIT 1"
                            connection.query(sql2, [link, hash, 1, result5[0].user_id], function (err, result56) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    sendResponse.sendSuccessData(constant.responseMessage.CHANGE_PASSWORD, res);
                                }
                            });
                        }
                        else {
                            sendResponse.sendErrorMessage(constant.responseMessage.SHOW_LINK_ERROR_MESSAGE, res);
                        }
                    }
                });
            } else if (result5.length == 0) {
                sendResponse.sendErrorMessage(constant.responseMessage.SHOW_LINK_ERROR_MESSAGE, res);
            }
            else {
                sendResponse.somethingWentWrongError(res);
            }
        });
    }
});

//Customer Change Password
router.post('/change_password', function (req, res) {
    var accessToken = req.body.access_token;
    var oldPassword = req.body.old_password;
    var newPassword = req.body.new_password;
    var values = [accessToken, oldPassword, newPassword];
    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id`FROM `tb_users` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {
                    var md5 = require('MD5');
                    var hash = md5(oldPassword);
                    var sql2 = "SELECT `user_id`FROM `tb_users` WHERE `access_token`=? AND `encryptPassword`=? LIMIT 1"
                    connection.query(sql2, [accessToken, hash], function (err, result_user) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            if (result_user.length == 0) {
                                sendResponse.sendErrorMessage(constant.responseMessage.PASSWORD_INCORRECT, res);
                            }
                            else {
                                var userId = result[0].user_id;
                                func.change_password_function(userId, oldPassword, newPassword, 1, req, res);
                            }
                        }
                    });
                }
            }
        });
    }
});

//Customer search drivers
router.post('/search_drivers', function (req, res) {
    var latitude1 = req.body.latitude;
    var longitude1 = req.body.longitude;
    var radius = req.body.radius;
    var values = [latitude1, longitude1, radius];
    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `latitude`,`longitude`FROM `tb_drivers` WHERE `verified`=?"
        connection.query(sql, [1], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result.length === 0) {
                    sendResponse.sendErrorMessage(constant.responseMessage.NO_DRIVER_AVAILABLE, res);
                }
                else {
                    var drivers = [];
                    var len = result.length;
                    for (var i = 0; i < len; i++) {
                        var temp = geolib.isPointInCircle(
                            {latitude: latitude1, longitude: longitude1},
                            {latitude: result[i].latitude, longitude: result[i].longitude},
                            radius
                        );
                        if (temp) {
                            drivers.push([result[i].latitude, result[i].longitude]);
                            //var sortedArray=geolib.orderByDistance({latitude: latitude1, longitude: longitude1},drivers);
                            //console.log(sortedArray);
                        }
                    }
                    if (drivers.length == 0) {
                        sendResponse.sendErrorMessage(constant.responseMessage.NO_DRIVER_AVAILABLE, res);
                    }
                    else {

                        sendResponse.sendSuccessData(drivers, res);
                    }
                }
            }
        });
    }
});

//Customer requests for a driver
router.post('/request_driver', function (req, res) {
    var latitude1 = req.body.latitude;
    var longitude1 = req.body.longitude;
    var radius = req.body.radius;
    var email = req.body.email;
    var values = [latitude1, longitude1, email];

    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `user_id` FROM `tb_users` WHERE `email`=?"
        connection.query(sql, [email], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {

                if (result.length == 0) {
                    sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_NOT_REGISTERED, res);
                }

                else {

                    var sql2 = "SELECT `latitude`,`longitude`,`driver_id`  FROM `tb_drivers` WHERE `verified`=?"
                    connection.query(sql2, [1], function (err, result2) {
                        if (err) {

                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            if (result2.length === 0) {
                                sendResponse.sendErrorMessage(constant.responseMessage.NO_DRIVER_AVAILABLE, res);

                            }
                            else {
                                var drivers = [];
                                var len = result2.length;
                                for (var i = 0; i < len; i++) {

                                    var temp = geolib.isPointInCircle(
                                        {latitude: latitude1, longitude: longitude1},
                                        {latitude: result2[i].latitude, longitude: result2[i].longitude},
                                        radius
                                    );

                                    if (temp) {
                                        drivers.push([result2[i].latitude, result2[i].longitude, result2[i].driver_id])
                                        //var sortedArray=geolib.orderByDistance({latitude: latitude1, longitude: longitude1},drivers);
                                    }

                                }
                                if (drivers.length == 0) {
                                    sendResponse.sendErrorMessage(constant.responseMessage.NO_DRIVER_AVAILABLE, res);
                                }
                                else {
                                    var sql3 = "INSERT INTO `tb_orders`(status,customer_id,latitude,longitude) VALUES(?,?,?,?)";
                                    connection.query(sql3, [1, result[0].user_id, latitude1, longitude1], function (err, result6) {
                                        if (err) {
                                            console.log(err);
                                            sendResponse.somethingWentWrongError(res);
                                        }
                                        else {
                                            var data = {
                                                customer_id: result[0].user_id,
                                                message: 'Please wait for confirmation from a particular driver',
                                                drivers: drivers
                                            };
                                            sendResponse.sendSuccessData(data, res);
                                        }
                                    });
                                }
                            }
                        }
                    });
                }
            }
        });
    }
});

//Driver signup
router.post('/driver_create', function (req, res) {
    var Fname = req.body.firstname;
    var Lname = req.body.lastname;
    var email = req.body.email;
    var password = req.body.password;
    var phone = req.body.phone;
    var deviceToken = req.body.device_token;
    var deviceType = req.body.device_type;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var checkBlank = [Lname, Fname, email, password, phone, deviceToken, deviceType, latitude, longitude];
    async.waterfall([
            function (callback) {
                func.checkBlankWithCallback(res, checkBlank, callback);

            },
            function (callback) {
                func.checkEmailAvailability(res, email, 0, callback);
            }
        ],
        function (updatePopup) {
            func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {
                var md5 = require('MD5');
                var hash = md5(password);
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + email;
                var accessToken = md5(accesstoken1);
                var loginTime = new Date();

                var sql = "INSERT into tb_drivers(Fname,Lname,email,encryptPassword,phone,access_token,date_registered,last_login,device_token,device_type,latitude,longitude,file_path,verified) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                connection.query(sql, [Fname, Lname, email, hash, phone, accessToken, loginTime, loginTime, deviceToken, deviceType, latitude, longitude, result_file, 0], function (err, result) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var sql = "SELECT * FROM tb_drivers WHERE email=? LIMIT 1";
                        connection.query(sql, [email], function (err, user, fields) {
                            var final = {
                                "Fname": user[0].Fname,
                                "Lname": user[0].Lname,
                                "email": user[0].email,
                                "user_id": user[0].user_id,
                                "last_login": user[0].last_login,
                                "password": user[0].encryptPassword,
                                "phone": user[0].phone,
                                "access_token": user[0].access_token,
                                "date_registered": user[0].date_registered,
                                "device_token": user[0].device_token,
                                "device_type": user[0].device_type,
                                "latitude": user[0].latitude,
                                "longitude": user[0].longitude
                            };
                            sendResponse.sendSuccessData(final, res);
                            var userName = user[0].Fname;
                            func.sendMailToCustomer(userName, email);
                        });
                    }
                });
            });
        });
});

//Driver login
router.post('/driver_login', function (req, res) {
    var user = req.body.user;
    var pass = req.body.pass;
    var check = [user, pass];
    var flag = func.checkBlank(check);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `verified` FROM `tb_drivers` WHERE `email`=? LIMIT 1"
        connection.query(sql, [user], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result[0].verified === 0) {
                sendResponse.sendErrorMessage(constant.responseMessage.DRIVER_NOT_VERIFIED, res);
            }
            else {
                func.email_login_function(user, pass, 0, res);
            }
        });
    }

});

//Driver access token login
router.post('/driver_access_token_login', function (req, res) {
    var accessToken = req.body.access_token;
    var manvalues = [accessToken];
    var checkdata = func.checkBlank(manvalues);
    if (checkdata === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id`,`Fname`,`Lname`,`file_path`,`phone`,`email`,`encryptPassword`,`date_registered`,`device_type`,`device_token` FROM `tb_drivers` WHERE `access_token`=? LIMIT 1 "
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length !== 0) {
                var md5 = require('MD5');
                var accesstoken = func.generateRandomString();
                var accesstoken1 = accesstoken + result[0].email;
                var accessToken2 = md5(accesstoken1);
                var sql2 = "UPDATE `tb_drivers` set `access_token`=?,`login_status`=? WHERE `driver_id`=? LIMIT 1 "
                connection.query(sql2, [accessToken2, 1, result[0].driver_id], function (err, resultUpdate) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var final = {
                            "user_name": result[0].Fname,
                            "user_image": result[0].user_image,
                            "access_token": result[0].access_token,
                            "email": result[0].email,
                            "phone_no": result[0].phone,
                            "device_token": result[0].device_token,
                            "device_type": result[0].device_type,
                            "user_id": result[0].user_id,
                            "last_login": result[0].last_login,
                            "password": result[0].encryptPassword,
                            "phone": result[0].phone,
                            "date_registered": result[0].date_registered,
                            "latitude": result[0].latitude,
                            "longitude": result[0].longitude
                        };
                        sendResponse.sendSuccessData(final, res);
                    }


                });
            }
            else {
                sendResponse.invalidAccessTokenError(res);
            }
        });
    }

});

//Driver logout
router.post('/driver_logout', function (req, res, next) {
    var accessToken = req.body.access_token;
    var flag = func.checkBlank(accessToken);
    if (flag) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `login_status` FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length === 0) {
                sendResponse.invalidAccessTokenError(res);
            }
            else {

                var sql = "UPDATE `tb_drivers` SET `login_status`=? WHERE `access_token`=? LIMIT 1";
                connection.query(sql, [0, accessToken], function (err, result) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        sendResponse.sendSuccessData(constant.responseMessage.SUCCESSFUL_LOGOUT, res);
                    }
                });
            }
        });
    }
});

//Driver edit profile
router.post('/driver_edit_profile', function (req, res) {
    var accessToken = req.body.access_token;
    var phone = req.body.phone;
    var email = req.body.email;
    var firstName = req.body.Fname;
    var lastName = req.body.Lname;
    var values = [accessToken, phone, firstName, lastName, email];
    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id`FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {

                    file = req.files.user_image;
                    filename = file.name;
                    remotePath = config.get("production").Url + filename;
                    filePath = config.get("production").Url;
                    if (remotePath === filePath) {

                        var sql1 = "UPDATE `tb_drivers` SET `Fname`=?,`Lname`=?,`phone`=?,`email`=? WHERE `driver_id`=? LIMIT 1"
                        connection.query(sql1, [firstName, lastName, phone, email, result[0].driver_id], function (err, result) {

                            if (err) {

                                sendResponse.somethingWentWrongError(res);
                            }
                            else {

                                sendResponse.sendSuccessData(constant.responseMessage.UPDATE_PROFILE, res);
                            }


                        });
                    }


                    else {

                        func.uploadFile(req.files.user_image, 'apurva_files', res, function (result_file) {

                            var sql2 = "UPDATE `tb_drivers` SET `file_path`=?,`Fname`=?,`Lname`=?,`phone`=?,`email`=?WHERE `driver_id`=? LIMIT 1 ";
                            connection.query(sql2, [result_file, firstName, lastName, phone, email, result[0].driver_id], function (err, result_update) {
                                if (err) {

                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    sendResponse.sendSuccessData(constant.responseMessage.UPDATE_PROFILE, res);
                                }
                            });

                        });
                    }
                }
            }
        });
    }
});

//Driver forgot password
router.post('/driver_forgot', function (req, res) {
    var email = req.body.email;

    var manValues = [email];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);

        },
        function (callback) {
            func.checkEmailRegistration(res, email, 0, callback);

        }], function (updatePopup) {

        var sql = "SELECT driver_id,Fname FROM tb_drivers WHERE email=? LIMIT 1";
        connection.query(sql, [email], function (err, response12) {

            if (response12.length === 1) {
                func.sendPasswordMailToCustomer(email, 0, res);

            }
            else {

                sendResponse.sendError(res);
            }

        });
    });
});

//Driver reset password
router.post('/driver_reset', function (req, res) {
    var link = req.body.link;
    var new_password = req.body.pass;
    var manValues = [link, new_password];
    var checkData = func.checkBlank(manValues);
    if (checkData === 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id` FROM `tb_drivers` WHERE `one_time_link`=? LIMIT 1"
        connection.query(sql, [link], function (err, result5) {
            if (result5) {
                var md5 = require('MD5');
                var hash = md5(new_password);
                var sql2 = "SELECT `password_reset` FROM `tb_drivers` WHERE `one_time_link`=? LIMIT 1"
                connection.query(sql2, [link], function (err, result57) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        if (result57.length == 0) {
                            sendResponse.sendSuccessData(constant.responseMessage.SHOW_LINK_ERROR_MESSAGE, res);
                        }
                        else if (result57[0].password_reset == 0) {
                            var sql3 = "UPDATE `tb_drivers` SET `one_time_link`=?,`encryptPassword`=?,`password_reset`=? WHERE `driver_id`=? LIMIT 1"
                            connection.query(sql3, [link, hash, 1, result5[0].driver_id], function (err, result56) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {

                                    sendResponse.sendSuccessData(constant.responseMessage.CHANGE_PASSWORD, res);
                                }
                            });
                        }
                        else {
                            sendResponse.sendErrorMessage(constant.responseMessage.LINK_EXPIRED, res);
                        }
                    }
                });
            }
            else {
                sendResponse.somethingWentWrongError(res);
            }
        });
    }
});

//Driver change password
router.post('/driver_change_password', function (req, res) {
    var accessToken = req.body.access_token;
    var oldPassword = req.body.old_password;
    var newPassword = req.body.new_password;
    var values = [accessToken, oldPassword, newPassword];
    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id`FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else {
                if (result == 0) {
                    sendResponse.invalidAccessTokenError(res);
                }
                else {
                    var md5 = require('MD5');
                    var hash = md5(oldPassword);
                    var sql2 = "SELECT `driver_id`FROM `tb_drivers` WHERE `access_token`=? AND `encryptPassword`=? LIMIT 1"
                    connection.query(sql2, [accessToken, hash], function (err, result_user) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            if (result_user.length == 0) {
                                sendResponse.sendErrorMessage(constant.responseMessage.PASSWORD_INCORRECT, res);
                            }
                            else {
                                var userId = result[0].driver_id;
                                func.change_password_function(userId, oldPassword, newPassword, 0, req, res);
                            }
                        }
                    });
                }
            }
        });
    }
});

//Driver accepts an order
router.post('/accept_order', function (req, res) {
    var email = req.body.email;
    var orderId = req.body.order_id;
    var values = [email, orderId];
    var checkData = func.checkBlank(values);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql1 = "SELECT `driver_id` FROM `tb_drivers` WHERE `email`=? LIMIT 1"
        connection.query(sql1, [email], function (err, result1) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result1.length == 0) {
                sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_NOT_REGISTERED, res);
            }
            else {
                var sql2 = "SELECT `status` FROM `tb_orders` WHERE `order_id`=? LIMIT 1"
                connection.query(sql2, [orderId], function (err, result2) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else if (result2.length == 0) {
                        sendResponse.sendErrorMessage(constant.responseMessage.NO_ORDER_REQUEST, res);
                    }
                    else if (result2[0].status == 2) {
                        sendResponse.sendErrorMessage(constant.responseMessage.ORDER_ALREADY_ASSIGNED, res);
                    }
                    else if (result2[0].status == 3) {
                        sendResponse.sendSuccessData(constant.responseMessage.ORDERED_COMPLETED, res);
                    }
                    else if (result2[0].status == 1) {

                        var sql3 = "UPDATE `tb_orders` SET `status`=?, `driver_id`=?  WHERE `order_id`=?";
                        connection.query(sql3, [2, result1[0].driver_id, orderId], function (err, result3) {
                            if (err) {
                                console.log(err);
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var sql4 = "SELECT * FROM `tb_orders` WHERE `order_id`=?";
                                connection.query(sql4, [orderId], function (err, result5) {
                                    if (err) {
                                        sendResponse.somethingWentWrongError(res);
                                    }
                                    else {
                                        var data = {
                                            customerId: result5[0].customer_id,
                                            driverId: result5[0].driver_id,
                                            orderId: result5[0].order_id
                                        };
                                        sendResponse.sendSuccessData(data, res);
                                    }
                                });
                            }
                        });
                    }
                });
            }

        });
    }
})
;

//Driver starts ride for delivering a particular order
router.post('/start_service', function (req, res) {
    var driverAccessToken = req.body.access_token;
    var orderId = req.body.order_id;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var checkVal = [driverAccessToken, orderId, latitude, longitude];
    var checkData = func.checkBlank(checkVal);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id` FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [driverAccessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length == 0) {
                sendResponse.invalidAccessTokenError(res);
            }
            else {
                var sql8 = "SELECT `status` FROM `tb_orders`WHERE `order_id`=?"
                connection.query(sql8, [orderId], function (err, result8) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else if (result8[0].status == 1) {
                        sendResponse.sendSuccessData(constant.responseMessage.ORDERED_NOT_CONFIRMED, res);
                    }
                    else if (result8[0].status == 3) {
                        sendResponse.sendSuccessData(constant.responseMessage.ORDERED_COMPLETED, res);
                    }
                    else {
                        var dates = new Date();
                        var sql2 = "UPDATE `tb_orders` SET `pickup_time`=? ,`pickup_latitude`=?,`pickup_longitude`=? WHERE `order_id`=? LIMIT 1"
                        connection.query(sql2, [dates, latitude, longitude, orderId], function (err, result2) {
                            if (err) {

                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var sql3 = "SELECT `customer_id` ,`driver_id`,`pickup_latitude`,`pickup_longitude`,`pickup_time`FROM `tb_orders` WHERE `order_id`=? LIMIT 1";
                                connection.query(sql3, [orderId], function (err, result3) {
                                    if (err) {
                                        console.log(err);
                                        sendResponse.somethingWentWrongError(res);
                                    }
                                    else {
                                        var data = {
                                            customerId: result3[0].customer_id,
                                            driverId: result3[0].driver_id,
                                            pickupLatitude: result3[0].pickup_latitude,
                                            pickupLongitude: result3[0].pickup_longitude,
                                            pickupTime: result3[0].pickup_time
                                        };
                                        sendResponse.sendSuccessData(data, res);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

//Driver ends ride after delivering a particular order
router.post('/end_service', function (req, res) {
    var driverAccessToken = req.body.access_token;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var orderId = req.body.order_id;
    var checkVal = [driverAccessToken, latitude, orderId, longitude];
    var checkData = func.checkBlank(checkVal);
    if (checkData == 1) {
        sendResponse.parameterMissingError(res);
    }
    else {
        var sql = "SELECT `driver_id` FROM `tb_drivers` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [driverAccessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length == 0) {
                sendResponse.invalidAccessTokenError(res);
            }
            else {
                var sql8 = "SELECT `status` FROM `tb_orders`WHERE `order_id`=?"
                connection.query(sql8, [orderId], function (err, result8) {
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else if (result8[0].status == 1) {
                        sendResponse.sendSuccessData(constant.responseMessage.ORDERED_NOT_CONFIRMED, res);
                    }
                    else if (result8[0].status == 3) {
                        sendResponse.sendSuccessData(constant.responseMessage.ORDERED_COMPLETED, res);
                    }
                    else {
                        var dates = new Date();
                        var sql2 = "UPDATE `tb_orders` SET `delivered_time`=? ,`delivered_latitude`=?,`delivered_longitude`=?,`status`=? WHERE `order_id`=? LIMIT 1"
                        connection.query(sql2, [dates, latitude, longitude, 3, orderId], function (err, result2) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var sql3 = "SELECT `customer_id` ,`driver_id`,`delivered_time` ,`delivered_latitude`,`delivered_longitude`FROM `tb_orders` WHERE `order_id`=?  LIMIT 1";
                                connection.query(sql3, [orderId], function (err, result3) {
                                    if (err) {
                                        sendResponse.somethingWentWrongError(res);
                                    }
                                    else {
                                        var data = {
                                            customerId: result3[0].customer_id,
                                            driverId: result3[0].driver_id,
                                            deliveredLatitude: result3[0].delivered_latitude,
                                            deliveredLongitude: result3[0].delivered_longitude,
                                            deliveredTime: result3[0].delivered_time
                                        };
                                        sendResponse.sendSuccessData(data, res);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});
module.exports = router;