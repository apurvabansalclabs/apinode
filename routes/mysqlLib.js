var mysql = require('mysql');
var config=require('config');
connection = mysql.createConnection({
    host: config.get("Database_Settings").host,
    user: config.get("Database_Settings").user,
    database: config.get("Database_Settings").database
});
connection.connect();