function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}

exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 101);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 102);
define(exports.responseStatus, "SHOW_MESSAGE", 103);
define(exports.responseStatus, "SHOW_DATA", 104);
define(exports.responseStatus, "INVALID_ACCESS_TOKEN", 105);

exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_ERROR_MESSAGE", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "SUCCESSFUL_EXECUTION", "Registration Successful.");
define(exports.responseMessage, "SUCCESSFUL_LOGOUT", "LOGOUT Successful.");
define(exports.responseMessage, "ALREADY_LOGGED_IN", "You're already logged in");
define(exports.responseMessage, "EMAIL_NOT_REGISTERED", "Oops your email has not been registered. Please sign up to login");
define(exports.responseMessage, "PASSWORD_INCORRECT", "Sorry, your password is incorrect");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN", "Invalid access token error");
define(exports.responseMessage, "MAIL_SENT", "Mail Sent Successfully.");
define(exports.responseMessage, "SHOW_LINK_ERROR_MESSAGE", "Link Invalid");
define(exports.responseMessage, "CHANGE_PASSWORD", "Password Changed Successfully.");
define(exports.responseMessage, "UPDATE_PROFILE", "Profile updated successfully");
define(exports.responseMessage, "DRIVER_NOT_VERIFIED", "Driver not verified");
define(exports.responseMessage, "NO_DRIVER_AVAILABLE", "Driver not available");
define(exports.responseMessage, "SHOW_DRIVERS", "Drivers available");
define(exports.responseMessage, "FILE_NOT_SELECTED", "File not selected");
define(exports.responseMessage, "NEW_ORDER_PLACED", "Order placed");
define(exports.responseMessage, "ORDERED_NOT_CONFIRMED", "Order not confirmed");
define(exports.responseMessage, "NO_ORDER_REQUEST", "There is no order request from this customer id");
define(exports.responseMessage, "LINK_EXPIRED","Sorry the link has expired");
define(exports.responseMessage, "ORDER_ALREADY_ASSIGNED","Order has already been assigned");
define(exports.responseMessage, "ORDERED_COMPLETED","Order has already been completed");