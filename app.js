sendResponse = require('./routes/sendResponse');
func = require('./routes/commonFunction');
constant=require('./routes/constant');
generatePassword = require('password-generator');
md5 = require('MD5');
config = require('config');
var express = require("express");
var app = express();
var connection = require('./routes/mySqlLib');
var index = require('./routes/index');
var users = require('./routes/users');
var bodyParser = require('body-parser');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('view engine', 'jade');

app.get('/', index);

//Customer side api
app.post('/create', multipartMiddleware);
app.post('/create', users);
app.post('/login', users);
app.post('/access_token_login', users);
app.post('/logout', users);
app.post('/edit_profile', multipartMiddleware);
app.post('/edit_profile', users);
app.post('/forgot', users);
app.post('/reset', users);
app.post('/change_password', users);
app.post('/request_driver', users);
app.post('/search_drivers', users);

//Driver side api
app.post('/driver_create', multipartMiddleware);
app.post('/driver_create', users);
app.post('/driver_login', users);
app.post('/driver_access_token_login', users);
app.post('/driver_logout', users);
app.post('/driver_edit_profile', multipartMiddleware);
app.post('/driver_edit_profile', users);
app.post('/driver_forgot', users);
app.post('/driver_reset', users);
app.post('/driver_change_password', users);
app.post('/accept_order', users);
app.post('/start_service', users);
app.post('/end_service', users);

app.listen(config.get('PORT'));
console.log("Express server started on port %d", config.get('PORT'));